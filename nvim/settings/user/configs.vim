" Basic configs 
syntax on   " Enable syntax highlighting
set number   " Show line numbers
set numberwidth=1   " Show line numbers with 1 digit
set relativenumber   " Show relative line numbers
set mouse=a   " Enable mouse support
set clipboard=unnamedplus   " Enable clipboard support
set showcmd   " Show command in status line
set ruler   " Show a ruler
set encoding=utf-8   " Set encoding
set showmatch   " Show matching brackets
set shiftwidth=4   " Set shiftwidth to 4
set tabstop=4   " Set tabstop to 4
set softtabstop=4   " Set softtabstop to 4
set smartindent    " Make indent smart again
set autoindent   " Autoindent
set expandtab   " Expand tabs to spaces
set laststatus=3   " Show status bar
set noshowmode   " Disable showmode
set scrolloff=8   " Set scrolloff to 8
set sidescrolloff=8   " Set sidescrolloff to 8
set nowrap   " Disable wrapping
set cursorline   " Highlight current line
set cmdheight=2   " Set cmdheight to 2
set signcolumn=auto   " Show signs in the right side of the screen
set hidden   " Hide buffers but let them open
set nobackup   " Disable backup files
set noswapfile   " Disable swapfile
set nowritebackup   " Disable writebackup
set updatetime=300   " Set update time to 300ms
set shortmess+=c   " Show messages in the status line
set nocompatible   " Disable vim compatibility
set nohlsearch   " highlight matches
set incsearch   " incremental searching
set ignorecase   " searches are case insensitive...
set smartcase   " ... unless they contain at least one capital letter Basic configurations
set splitright   " Split buffers to the right
set splitbelow   " Split buffers below
set emoji   " Enable emojis
set lazyredraw   " Dont redraw the whole screen
set colorcolumn=81