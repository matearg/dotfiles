"              /$$                                  
"             |__/                                  
"   /$$    /$$ /$$ /$$$$$$/$$$$   /$$$$$$   /$$$$$$$
"  |  $$  /$$/| $$| $$_  $$_  $$ /$$__  $$ /$$_____/
"   \  $$/$$/ | $$| $$ \ $$ \ $$| $$  \__/| $$      
"    \  $$$/  | $$| $$ | $$ | $$| $$      | $$      
"  /$$\  $/   | $$| $$ | $$ | $$| $$      |  $$$$$$$
" |__/ \_/    |__/|__/ |__/ |__/|__/       \_______/

" User configs
source ~/github/dotfiles/nvim/settings/user/configs.vim
source ~/github/dotfiles/nvim/settings/user/keybinds.lua

" Vimplug configs
source ~/github/dotfiles/nvim/settings/plugins/plugins.vim

" Colorscheme configs
source ~/github/dotfiles/nvim/settings/colors/color-configs.lua
source ~/github/dotfiles/nvim/settings/colors/colorscheme.vim

" Plugins configs
source ~/github/dotfiles/nvim/settings/plugins/configs/alpha.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/bufferline.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/coc.vim
source ~/github/dotfiles/nvim/settings/plugins/configs/floaterm.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/gitgutter.vim
source ~/github/dotfiles/nvim/settings/plugins/configs/indentline.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/lualine.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/nvimtree.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/project.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/telescope.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/treesitter.lua
source ~/github/dotfiles/nvim/settings/plugins/configs/whichkey.lua

" Not in use
" source ~/github/dotfiles/nvim/settings/plugins/configs/syntastic.vim
" source ~/github/dotfiles/nvim/settings/plugins/configs/polyglot.vim
